import requests
import json
import time
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
 
from poolModels import pool, poolBase
 
engine = create_engine('sqlite:///poolData.db')
poolBase.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()
 
def is_json(myjson):
  try:
    json_object = json.loads(myjson)
  except ValueError:
    return False
  return True

def main():
    allPools = session.query(pool).all()
    totalPools = len(allPools)
    for poolNumber in range(0,totalPools):
        poolName = allPools[poolNumber].name
        poolURL = allPools[poolNumber].url
        poolType = allPools[poolNumber].type
        print("INFO: Getting data for pool {} at URL {}".format(poolName, poolURL))
        response = requests.get(poolURL)
        data = response.json()
        print(data)
        print is_json(data)
        if poolType == "normal":
            dataLocation = "pool"
            keyHashrate = "hashrate"
        else:
            dataLocation = "pool_statistics"
            keyHashrate = "hashRate"       
        poolData = data[dataLocation]
        poolMiners = int(poolData["miners"])
        poolHashrate = int(poolData[keyHashrate])
        allPools[poolNumber].miners = poolMiners
        allPools[poolNumber].hashrate = poolHashrate   
        print("DATA: {} miners {}".format(poolName, poolMiners))
        print("DATA: {} hashrate {}".format(poolName, poolHashrate))    
    session.commit()

while 1:
    try:
        main()
        print("INFO: 60 second pause")
        time.sleep(60)
    except:
        continue